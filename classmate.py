"""issue categorization test"""

import json
import numpy as np

from sklearn.feature_extraction.text import TfidfVectorizer

issues = []

with open('input.json', 'r') as issue_file:
    issues = json.load(issue_file)

SIMILARITY_THRESHOLD = 0.3


def tfidf_similarity(X, Y=None):
    if Y is None:
        Y = X.T
    matrix = (X * Y).A
    # matrix = pairwise_distances(X, Y, metric='cosine')
    # matrix = pairwise_distances(X, Y, metric='euclidean')
    return matrix


issue_numbers = [issue['id'] for issue in issues]
issue_texts = [issue['text'] for issue in issues]

vect = TfidfVectorizer(min_df=0, ngram_range=(1, 5))
tfidf = vect.fit_transform(issue_texts)
matrix = tfidf_similarity(tfidf)

# Map issue_numbers with respective scores
results = [zip(issue_numbers, scores) for scores in matrix]
# Filter


results = [dict(filter(lambda r: r[1] >= SIMILARITY_THRESHOLD, row)) for row in results]
# Map (key=issue_number, value=result)
results = dict(zip(issue_numbers, results))
# Clean results
for issue_number in issue_numbers:
    # Remove similarity with itself
    if issue_number in results and issue_number in results[issue_number]:
        del results[issue_number][issue_number]
    # Delete empty entries
    if issue_number in results and not results[issue_number]:
        del results[issue_number]
# And we're done!




texts = {}
for issue in issues:
    texts[issue['id']] = issue['text']


# print(results)
printed = []
for n in issue_numbers:
    if n not in results:
        # print('single:', n)
        pass
    elif n in printed:
        continue
    else:
        print(n, texts[n])
        printed.append(n)

        for nn in results[n]:
            if nn not in printed:
                print(nn, texts[nn])
                printed.append(nn)

        print('---')

